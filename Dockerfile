FROM mongo:8.0@sha256:f6164e498dbaee3966031c1ced1bfa0fd1c4961151b38a770fa6994f4b0dcae7

COPY files/ /

ENV MONGO_REPLICASET_KEY_CONTENTS="" \
    MONGO_REPLICASET_KEY_FILENAME="" \
    MONGO_REPLICASET_KEY_DEFAULT_FILENAME="/opt/mongo/replica.key" \
    MONGO_REPLICASET_NAME=""

ENTRYPOINT ["/usr/local/sbin/craynic-entrypoint.sh"]
CMD ["mongod"]
