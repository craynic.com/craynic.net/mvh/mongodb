#!/usr/bin/env bash

set -Eeuo pipefail

KEY_FILENAME="${MONGO_REPLICASET_KEY_FILENAME:-$MONGO_REPLICASET_KEY_DEFAULT_FILENAME}"

if [[ -f "$KEY_FILENAME" ]]; then
  echo "Replica key \"$KEY_FILENAME\" exists, using it, ok."
  exit 0
fi

if [[ -z "$MONGO_REPLICASET_KEY_FILENAME" && -z "$MONGO_REPLICASET_KEY_CONTENTS" ]]; then
  echo "Neither replica key filename nor contents were specified, not using replica set key authentication, ok."
  exit 0
fi

if [[ -z "$MONGO_REPLICASET_KEY_CONTENTS" ]]; then
  echo "Replica key file does not exist and the contents was not defined, generating a new key..."

  set +o pipefail
  MONGO_REPLICASET_KEY_CONTENTS=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 1024)
  set -o pipefail
fi

echo "$MONGO_REPLICASET_KEY_CONTENTS" > "$KEY_FILENAME"
chown mongodb:mongodb -- "$KEY_FILENAME"
chmod u-wx,go-rwx -- "$KEY_FILENAME"

echo "Created new replica key file \"$KEY_FILENAME\"."
