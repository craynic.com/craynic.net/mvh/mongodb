#!/usr/bin/env bash

set -Eeuo pipefail

# run all init scripts
run-parts --exit-on-error --regex='\.sh$' -- "/docker-init.d/"

ARGUMENTS=("$@")

if [[ -n "$MONGO_REPLICASET_NAME" ]]; then
  ARGUMENTS+=("--replSet" "$MONGO_REPLICASET_NAME")

  KEY_FILENAME="${MONGO_REPLICASET_KEY_FILENAME:-$MONGO_REPLICASET_KEY_DEFAULT_FILENAME}"

  if [[ -f "$KEY_FILENAME" ]]; then
    ARGUMENTS+=("--keyFile" "$KEY_FILENAME")
  fi
fi

# run
docker-entrypoint.sh "${ARGUMENTS[@]}"
